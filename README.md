# Prusa Slicer Profiles
## List of contents
In this repository you can find the folowing:

* B2X300 Profiles for Prusa Slicer 2.1.0
* Prusa Slicer 2.1.0 for Windows 64 bit with profiles included
* Prusa Slicer 2.1.0 for Windows 32 bit without profiles included
* Prusa Slicer 2.1.0 for Linux 64 bit without profiles included
* Prusa Slicer 2.1.0 for MacOS without profiles included
* Tutorial on how to install and use slicer with B2X300

## Instructions
Please follow the pdf file inside the repository in order to learn how to install and use PrusaSlicer 2.1.0.
If there are any repeated files with different dates in the name ALWAYS use the latest version.